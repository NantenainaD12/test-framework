/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Utilitaire.ModelView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author NAINA
 */
public class Departement {
    int id;
    String nom;
    static List<Departement> all=new ArrayList<>();
    
    
    @URLAnnotation(url="insert_dept.do")
    public  ModelView create(){
        System.out.println("Tonga dept ehh");
        HashMap<String, Object>Hval=new HashMap<>();
        List<Departement> all=get_all();
        all.add(this);
        ModelView md=new ModelView();
        Hval.put("liste_Departement", all);
        md.setData(Hval);
        md.setPage("liste_Departement.jsp");
    return md;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @URLAnnotation(url="liste_Departement.do")
    public static ModelView liste(){
    System.out.println("Tonga ehh");
    HashMap<String, Object>Hval=new HashMap<>();
    List<Departement> all=Departement.get_all();
    ModelView md=new ModelView();
        Hval.put("liste_Departement", all);
        md.setData(Hval);
        md.setPage("liste_Departement.jsp");
    return md;
    }
    public static List<Departement> get_all()
    {
        all.add(new Departement(1,"amphi A"));
        all.add(new Departement(2, "Grande salle"));
        all.add(new Departement(3, "amphi B"));
        
        return all;
    }

    public Departement() {
    }
    

    public  Departement(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
}
