/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Utilitaire.ModelView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author NAINA
 */
public class Emp {
    int idEMP;
    String nom;
    static List<Emp> all=new ArrayList<>();

    @URLAnnotation(url="insertEmp.do")
    public  ModelView create(){
        HashMap<String, Object>Hval=new HashMap<>();
        List<Emp> all=get_all();
        all.add(this);
        ModelView md=new ModelView();
        Hval.put("liste_Emp", all);
        md.setData(Hval);
        md.setPage("liste_Emp.jsp");
    return md;
    }

////Fonction just liste
    @URLAnnotation(url="liste_emp.do")
    public ModelView get_Emp(){
        System.out.println("Tonga ehh");
        HashMap<String, Object>Hval=new HashMap<>();
        List<Emp> ll=get_all();
        ModelView md=new ModelView();
        Hval.put("liste_Emp", ll);
        md.setData(Hval);
        md.setPage("liste_Emp.jsp");
    return md;
    }
    public static List<Emp> get_all()
    {
        all.add(new Emp(1,"Koto be"));
        all.add(new Emp(2, "Jean koto"));
        all.add(new Emp(3, "Lita"));
        
        return all;
    }

    public Emp() {
    }

    public Emp(int idEMP, String nom) {
        this.idEMP = idEMP;
        this.nom = nom;
    }

    public int getidEMP() {
        return idEMP;
    }

    public void setidEMP(int idEMP) {
        this.idEMP = idEMP;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
}
