<%-- 
    Document   : liste_dept
    Created on : 7 nov. 2022, 19:31:11
    Author     : NAINA
--%>
<%@page import="Model.*" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.List" %>
<%
    HashMap<String, Object>valiny=(HashMap<String, Object>)request.getAttribute("data");
    List<Departement> all=(List<Departement>)valiny.get("liste_Departement");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <h1>Liste Departement</h1>
    <body>  
          <table>
            <tr>
                <th>ID</th>
                <th>Nom</th>
            </tr>
                    <%for (Departement departement : all) {%>
                      <tr>
                        <td> <% out.print(departement.getId());%></td>
                        <td> <% out.print(departement.getNom());%></td>
                      </tr>
                    <%}%>
    </table>
    </body>
</html>
